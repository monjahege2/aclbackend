package com.example.projektarbeit.api.vertraege;

import com.example.projektarbeit.api.acl.authentication.PermissionAssignmentService;
import com.example.projektarbeit.api.repository.VertraegeRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class VertraegeRestController {
    private VertraegeRepository vertraegeRepository;
    private PermissionAssignmentService permissionAssignmentService;

    public VertraegeRestController(VertraegeRepository vertraegeRepository, PermissionAssignmentService permissionAssignmentService) {
        this.vertraegeRepository = vertraegeRepository;
        this.permissionAssignmentService = permissionAssignmentService;
    }

    @PostAuthorize("hasAnyRole('ROLE_ADMIN','ROLE_Projekt 1', 'ROLE_Projekt 2', 'ROLE_Projekt 1_MA', 'ROLE_Projekt 2_MA')")
    @GetMapping("/vertrag/{id}")
    public ResponseEntity<Vertrag> getVertragByID(@PathVariable Integer id) {
        Vertrag vertrag = vertraegeRepository.findById(id).orElseThrow();
        return new ResponseEntity<>(vertrag, HttpStatus.OK);
    }

    @GetMapping("/vertrag")
    public ResponseEntity<List<Vertrag>> getAllVertraege() {
        List<Vertrag> vertrag = vertraegeRepository.findAll();
        return new ResponseEntity<>(vertrag, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_Projekt 1', 'ROLE_Projekt 2', 'ROLE_ADMIN')")
    @DeleteMapping("/vertrag/{id}")
    public ResponseEntity deleteVertragById(@PathVariable Integer id) {
        vertraegeRepository.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasPermission(#vertrag, 'WRITE')")
    @PutMapping("/vertrag")
    public ResponseEntity<Vertrag> updateVertrag(@RequestBody @Param("vertrag") Vertrag vertrag) {
        vertraegeRepository.save(vertrag);
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @Secured({"ROLE_Projekt 1", "ROLE_Projekt 2", "ROLE_ADMIN", "RUN_AS_ADMIN"})
    @PostMapping("/vertrag")
    public ResponseEntity<Vertrag> saveVertrag(@RequestBody Vertrag vertrag) {
        vertraegeRepository.save(vertrag);
        permissionAssignmentService.createVertragPermission(vertrag);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
