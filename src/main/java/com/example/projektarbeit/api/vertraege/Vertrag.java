package com.example.projektarbeit.api.vertraege;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class Vertrag {

    public Vertrag(){};

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;

    @NotNull
    private int projekt;
    private String notiz;

    public Vertrag(String name) {
        this.name = name;
    }

    public Integer getId() {
        return this.id;
    }



}
