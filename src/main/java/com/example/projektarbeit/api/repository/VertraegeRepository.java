package com.example.projektarbeit.api.repository;

import com.example.projektarbeit.api.vertraege.Vertrag;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface VertraegeRepository extends CrudRepository<Vertrag, Integer> {

    @Transactional
    void deleteById(Integer id);

    @PostAuthorize("hasPermission(returnObject.orElseThrow(), 'READ')")
    Optional<Vertrag> findById(Integer id);

    @PostFilter("hasPermission(filterObject, 'READ')")
    List<Vertrag> findAll();

    Vertrag save(@Param("vertrag") Vertrag vertrag);


}
