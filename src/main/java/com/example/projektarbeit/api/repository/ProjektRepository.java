package com.example.projektarbeit.api.repository;

import com.example.projektarbeit.api.projekte.Projekt;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjektRepository extends CrudRepository<Projekt, Long> {
    Projekt findById(Integer id);

    List<Projekt> findAllByName(String name);

    List<Projekt> findAll();

    Projekt save(@Param("projekt") Projekt projekt);
}
