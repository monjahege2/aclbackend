package com.example.projektarbeit.api.repository;

import com.example.projektarbeit.api.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {

    User findByName(String name);

    List<User> findAll();

    User findById(int id);
}
