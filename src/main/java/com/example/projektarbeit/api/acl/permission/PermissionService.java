package com.example.projektarbeit.api.acl.permission;

import com.example.projektarbeit.api.vertraege.Vertrag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.acls.AclPermissionEvaluator;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.model.Permission;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@Service
@RequiredArgsConstructor
public class PermissionService {

    private final AclPermissionEvaluator aclPermissionEvaluator;

    public boolean checkPermission(@NotNull WriteType writeType, @NotNull Vertrag vertrag) throws NullPointerException {
        Serializable id = retrieveIdOfObject(vertrag);
        switch (writeType) {
            case UPDATE:
                return hasWritePermission(vertrag.getClass().getName(), id);
            case DELETE:
                return hasDeletePermission(vertrag.getClass().getName(), id);
            case READ:
                return hasReadPermission(vertrag.getClass().getName(), id);
            default:
                return false;
        }
    }

    private boolean hasWritePermission(@NotNull String className, Serializable id) {
        return hasPermission(className, BasePermission.WRITE, id);
    }

    // Delete only concerns complete instances, not fields
    private boolean hasDeletePermission(@NotNull String className, Serializable id) {
        return hasPermission(className, BasePermission.DELETE, id);
    }

    private boolean hasReadPermission(@NotNull String className, Serializable id) {
        return hasPermission(className, BasePermission.READ, id);
    }

    /**
     * Checks permissions. Provide an object to check permission against.
     *
     * @param className  The name of the class to create permissions for.
     * @param permission Which permission to check for (Read, Write, etc.)
     * @param id         The instance id, required when checking for permissions of instance level
     * @return
     */
    private boolean hasPermission(@NotNull String className, @NotNull Permission permission, Serializable id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        return aclPermissionEvaluator.hasPermission(auth, id, className, permission);

    }

    public static Serializable retrieveIdOfObject(Object object) throws RuntimeException {
        Class<?> typeClass = ClassUtils.getUserClass(object.getClass());
        Serializable identifier = null;
        try {
            Method method = typeClass.getMethod("getId", new Class[] {});
            identifier = (Serializable) method.invoke(object);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException("Instance has no identifier");
        }
        return identifier;
    }

}
