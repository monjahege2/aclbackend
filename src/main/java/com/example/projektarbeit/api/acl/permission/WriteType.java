package com.example.projektarbeit.api.acl.permission;

public enum WriteType {
        UPDATE, DELETE, READ
}
