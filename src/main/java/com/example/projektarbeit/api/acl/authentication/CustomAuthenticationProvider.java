package com.example.projektarbeit.api.acl.authentication;

import com.example.projektarbeit.api.repository.UserRepository;
import com.example.projektarbeit.api.user.User;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

    private String username;
    private String password;
    private User user;
    private String role;

    private UserRepository userRepository;

    public CustomAuthenticationProvider(UserRepository userRepository) {this.userRepository = userRepository;}


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        this.username = authentication.getName();
        this.password = authentication.getCredentials().toString();
        try {
            this.user = userRepository.findByName(username);
            role = user.getRole();
        } catch (Exception e) { return null;}
        GrantedAuthorityImpl grantedAuthority = new GrantedAuthorityImpl();
        if(role != null) {
            grantedAuthority.setAuthority(role);
            return  setUserToken(Collections.singletonList(grantedAuthority));
        } else {
            return setUserToken(Collections.EMPTY_LIST);
        }
    }

    private UsernamePasswordAuthenticationToken setUserToken(Collection<GrantedAuthorityImpl> collection) {
        if (username.equalsIgnoreCase(user.getName()) && password.equals(user.getPasswort())) {
            return new UsernamePasswordAuthenticationToken
                    (username, password, collection);
        } else {
            return null;
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
