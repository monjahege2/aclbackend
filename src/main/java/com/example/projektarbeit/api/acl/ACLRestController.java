package com.example.projektarbeit.api.acl;

import com.example.projektarbeit.api.acl.permission.PermissionService;
import com.example.projektarbeit.api.acl.permission.WriteType;
import com.example.projektarbeit.api.repository.VertraegeRepository;
import com.example.projektarbeit.api.vertraege.Vertrag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
@Component
@RestController
public class ACLRestController {

    @Autowired
    PermissionService permissionService;
    @Autowired
    VertraegeRepository vertraegeRepository;

    @PersistenceContext
    EntityManager em;

    @GetMapping("/roles")
    public ResponseEntity getAllRoles() {
        Query q = em.createNativeQuery("SELECT sid FROM acl_sid");
        return new ResponseEntity<>(q.getResultList(), HttpStatus.OK);
    }

    @GetMapping("/hasPermission/{writeType}/{id}")
    public ResponseEntity<Boolean> getHasPermission(@PathVariable Integer writeType, @PathVariable Integer id) {
        Vertrag vertrag = vertraegeRepository.findById(id).orElseThrow();
        WriteType writeType1;
        switch (writeType){
            case 0: writeType1 = WriteType.READ;
            break;
            case 1: writeType1 = WriteType.UPDATE;
            break;
            case 2: writeType1 = WriteType.DELETE;
            break;
            default: writeType1 = null;
        }
        return new ResponseEntity<>(this.permissionService.checkPermission(writeType1, vertrag), HttpStatus.OK);
    }
}
