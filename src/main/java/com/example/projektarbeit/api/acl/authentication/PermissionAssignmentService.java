package com.example.projektarbeit.api.acl.authentication;

import com.example.projektarbeit.api.projekte.Projekt;
import com.example.projektarbeit.api.repository.ProjektRepository;
import com.example.projektarbeit.api.vertraege.Vertrag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.acls.domain.BasePermission;
import org.springframework.security.acls.domain.GrantedAuthoritySid;
import org.springframework.security.acls.domain.ObjectIdentityImpl;
import org.springframework.security.acls.jdbc.JdbcMutableAclService;
import org.springframework.security.acls.model.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotNull;

@Service
@RequiredArgsConstructor
public class PermissionAssignmentService {

    private final JdbcMutableAclService aclService;
    @Autowired
    private ProjektRepository projektRepository;

    private Sid role;
    private MutableAcl acl;

    @Transactional
    public void createVertragPermission(@NotNull Vertrag vertrag) {
        createVertragPermissionImpl(vertrag);
    }

    private void createVertragPermissionImpl(@NotNull Vertrag vertrag) {
        // Prepare the information we'd like in our access control entry (ACE)
        ObjectIdentity objectIdentity = new ObjectIdentityImpl(vertrag.getClass(), vertrag.getId());

        Projekt projekt = projektRepository.findById(vertrag.getProjekt());
        String projektName = projekt.getName();
        role = new GrantedAuthoritySid("ROLE_" + projektName);

        Permission read = BasePermission.READ;
        Permission write = BasePermission.WRITE;

        // Create or update the relevant ACL
        try {
            acl = (MutableAcl) aclService.readAclById(objectIdentity);
        } catch (NotFoundException nfe) {
            acl = aclService.createAcl(objectIdentity);
        }

        // Now grant some permissions via an access control entry (ACE)
        acl.setOwner(role);
        insertInAcl(role, read, write);

        role = new GrantedAuthoritySid("ROLE_" + projektName + "_MA");
        insertInAcl(role, read);

        role = new GrantedAuthoritySid("ROLE_ADMIN");
        insertInAcl(role, read, write);

        aclService.updateAcl(acl);
    }

    private void insertInAcl(Sid role, Permission... permissions) {
        for(Permission permission: permissions) {
            acl.insertAce(acl.getEntries().size(), permission, role, true);
        }
    }
}
