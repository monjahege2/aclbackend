package com.example.projektarbeit.api.acl.authentication;

import org.springframework.security.core.GrantedAuthority;

public class GrantedAuthorityImpl implements GrantedAuthority {
    private String auth;

    @Override
    public String getAuthority() {
        return auth;
    }

    public void setAuthority(String auth) {
        this.auth = auth;
    }
}
