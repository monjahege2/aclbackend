package com.example.projektarbeit.api.user;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class User {

    public User(){}

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String passwort;
    private String role;

    public User(String name, String passwort) {
        this.name = name;
        this.passwort = passwort;
    }
}
