package com.example.projektarbeit.api.projekte;

import com.example.projektarbeit.api.repository.ProjektRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ProjektRestController {

    private ProjektRepository projektRepository;

    public ProjektRestController(ProjektRepository projektRepository) {
        this.projektRepository = projektRepository;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_Projekt 1', 'ROLE_Projekt 2', 'ROLE_Projekt 1_MA', 'ROLE_Projekt 2_MA')")
    @GetMapping("/projekt/{id}")
    public ResponseEntity<Projekt> getProjektByID(@PathVariable Integer id) {
        Projekt projekt = projektRepository.findById(id);
        return new ResponseEntity<>(projekt, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_Projekt 1', 'ROLE_Projekt 2')")
    @GetMapping("/projektByName/{name}")
    public ResponseEntity<List<Projekt>> getAllProjekteByName(@PathVariable String name) {
        List<Projekt> projekte = projektRepository.findAllByName(name);
        return new ResponseEntity<>(projekte, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_Projekt 1', 'ROLE_Projekt 2')")
    @GetMapping("/projekt")
    public ResponseEntity<List<Projekt>> getAllProjekte() {
        List<Projekt> projekt = projektRepository.findAll();
        return new ResponseEntity<>(projekt, HttpStatus.OK);
    }
}
