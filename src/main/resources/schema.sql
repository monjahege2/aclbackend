CREATE TABLE IF NOT EXISTS acl_sid (
                                       id bigint(20) NOT NULL AUTO_INCREMENT,
                                       principal tinyint(1) NOT NULL,
                                       sid varchar(100) NOT NULL,
                                       PRIMARY KEY (id),
                                       UNIQUE KEY unique_uk_1 (sid,principal)
);

CREATE TABLE IF NOT EXISTS acl_class (
                                         id bigint(20) NOT NULL AUTO_INCREMENT,
                                         class varchar(255) NOT NULL,
                                         PRIMARY KEY (id),
                                         UNIQUE KEY unique_uk_2 (class)
);

CREATE TABLE IF NOT EXISTS acl_entry (
                                         id bigint(20) NOT NULL AUTO_INCREMENT,
                                         acl_object_identity bigint(20) NOT NULL,
                                         ace_order int(11) NOT NULL,
                                         sid bigint(20) NOT NULL,
                                         mask int(11) NOT NULL,
                                         granting tinyint(1) NOT NULL,
                                         audit_success tinyint(1) NOT NULL,
                                         audit_failure tinyint(1) NOT NULL,
                                         PRIMARY KEY (id),
                                         UNIQUE KEY unique_uk_4 (acl_object_identity,ace_order)
);

CREATE TABLE IF NOT EXISTS acl_object_identity (
                                                   id bigint(20) NOT NULL AUTO_INCREMENT,
                                                   object_id_class bigint(20) NOT NULL,
                                                   object_id_identity bigint(20) NOT NULL,
                                                   parent_object bigint(20) DEFAULT NULL,
                                                   owner_sid bigint(20) DEFAULT NULL,
                                                   entries_inheriting tinyint(1) NOT NULL,
                                                   PRIMARY KEY (id),
                                                   UNIQUE KEY unique_uk_3 (object_id_class,object_id_identity)
);

create table if not exists Projekt
(
    id integer not null auto_increment,
    name varchar(255) not null ,
    projektnummer varchar(255) not null
);

create table if not exists vertrag
(
    id integer not null auto_increment,
    name varchar(255) not null ,
    notiz varchar(255) default null,
    projekt integer not null
);

create table if not exists User
(
    id integer not null auto_increment,
    name varchar(255) not null ,
    passwort varchar(255) not null,
    role varchar(255)
);

create table if not exists UserSID
(
    userId integer not null,
    sid integer not null
);

ALTER TABLE acl_entry
    ADD FOREIGN KEY (acl_object_identity) REFERENCES acl_object_identity(id);

ALTER TABLE acl_entry
    ADD FOREIGN KEY (sid) REFERENCES acl_sid(id);

-- ALTER TABLE User
--     ADD FOREIGN KEY (acl_sid) REFERENCES acl_sid (id);

--
-- Constraints for table acl_object_identity
--
ALTER TABLE acl_object_identity
    ADD FOREIGN KEY (parent_object) REFERENCES acl_object_identity (id);

ALTER TABLE acl_object_identity
    ADD FOREIGN KEY (object_id_class) REFERENCES acl_class (id);

ALTER TABLE acl_object_identity
    ADD FOREIGN KEY (owner_sid) REFERENCES acl_sid (id);

ALTER TABLE vertrag
    ADD FOREIGN KEY (projekt) REFERENCES Projekt (id);

-- Values:
insert into Projekt(id, name, projektnummer) values
(1, 'Projekt 1', 'PR1'),
(2, 'Projekt 2', 'PR2'),
(3, 'Projekt 3', 'PR3'),
(4, 'Projekt 4', 'PR4');

insert into vertrag(id, name, notiz, projekt) values
(1, 'Test', null, 1);

INSERT INTO acl_sid (id, principal, sid) VALUES
(1, 0, 'ROLE_ADMIN'),
(2, 0, 'ROLE_HR'),
(3, 0, 'ROLE_HR_PR'),
(4, 0, 'ROLE_Projekt 1'),
(5, 0, 'ROLE_Projekt 2'),
(6, 0, 'ROLE_Projekt 1_MA'),
(7, 0, 'ROLE_Projekt 2_MA');

insert into User(name, passwort, role) values
-- ('nichts','nichts', null),
('monja', '123', 'ROLE_ADMIN'),
('PO', 'p', 'ROLE_Projekt 1'),
('PO MA', 'p', 'ROLE_Projekt 1_MA'),
('HR', 'h', 'ROLE_HR'),
('HR PR', 'h', 'ROLE_HR_PR');

INSERT INTO acl_class (id, class) VALUES
(1, 'com.example.projektarbeit.api.vertraege.Vertrag');

INSERT INTO acl_object_identity
(id, object_id_class, object_id_identity,
 parent_object, owner_sid, entries_inheriting)
VALUES
(1, 1, 1, NULL, 3, 0),
(2, 1, 2, NULL, 3, 0),
(3, 1, 3, NULL, 3, 0),
(4, 1, 4, NULL, 3, 0),
(5, 1, 5, NULL, 3, 0);
-- (6, 1, 6, NULL, 3, 0);

-- INSERT INTO acl_entry
-- (id, acl_object_identity, ace_order,
--  sid, mask, granting, audit_success, audit_failure)
-- VALUES
-- (1, 1, 1, 1, 1, 1, 1, 1),
-- (2, 1, 2, 1, 2, 1, 1, 1),
-- (3, 1, 2, 1, 2, 1, 1, 1),
-- (2, 1, 2, 1, 2, 1, 1, 1),
-- (2, 1, 2, 1, 2, 1, 1, 1),
-- (2, 1, 2, 1, 2, 1, 1, 1),
-- (2, 1, 2, 1, 2, 1, 1, 1),
-- (2, 1, 2, 1, 2, 1, 1, 1),
-- (3, 1, 3, 3, 1, 1, 1, 1),
-- (4, 2, 1, 2, 1, 1, 1, 1),
-- (5, 2, 2, 3, 1, 1, 1, 1),
-- (6, 3, 1, 3, 1, 1, 1, 1),
-- (7, 3, 2, 3, 2, 1, 1, 1),
-- -- (8, 2, 3, 4, 1, 1, 1, 1),
-- -- (9, 3, 3, 4, 1, 1, 1, 1),
-- -- (10, 3, 4, 4, 2, 1, 1, 1),
-- (11, 1, 4, 4, 1, 1, 1, 1),
-- (12, 1, 5, 6, 1, 1, 1, 1),
-- (13, 1, 6, 6, 2, 1, 1, 1),
-- (14, 2, 3, 6, 1, 1, 1, 1),
-- (15, 2, 4, 6, 2, 1, 1, 1);
