package com.example.projektarbeit;

import com.example.projektarbeit.api.repository.VertraegeRepository;
import com.example.projektarbeit.api.vertraege.VertraegeRestController;
import com.example.projektarbeit.api.vertraege.Vertrag;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class ProjektarbeitApplicationTests {

	@Autowired
	VertraegeRepository repo;

	@Autowired
	VertraegeRestController vertraegeRestController;

	private static int FIRST_VERTRAG_ID = 1;
	private static int SECOND_VERTRAG_ID = 2;

	@Test
	void contextLoads() {
	}

	@Test
	@WithMockUser(username = "nichts")
	public void
	givenUserManager_whenFindAllVertraege_thenReturnFirstMessage(){
		List<Vertrag> vertraege = repo.findAll();

		Assert.assertNotNull(vertraege);
		Assert.assertEquals(1,vertraege.size());
		Assert.assertEquals(java.util.Optional.ofNullable(FIRST_VERTRAG_ID), java.util.Optional.ofNullable(vertraege.get(0).getId()));
	}

	@Test
	@WithMockUser(roles = {"EDITOR"})
	public void
	givenRoleEditor_whenFindAllVertraege_thenReturn3Message(){
		List<Vertrag> vertraege = repo.findAll();

		Assert.assertNotNull(vertraege);
		Assert.assertEquals(3,vertraege.size());
	}

	@Test
	@WithMockUser(username = "nichts")
	public void
	givenUserManager_whenFind1stMessageByIdAndUpdateItsContent_thenOK(){

		Vertrag vertrag = repo.findById(FIRST_VERTRAG_ID).orElseThrow();
		Assert.assertNotNull(vertrag);
		Assert.assertEquals(java.util.Optional.ofNullable(FIRST_VERTRAG_ID), java.util.Optional.ofNullable(vertrag.getId()));

		vertrag.setProjekt(1);
		repo.save(vertrag);

		Vertrag editedFirstMessage = repo.findById(FIRST_VERTRAG_ID).orElseThrow();

		Assert.assertNotNull(editedFirstMessage);
		Assert.assertEquals(java.util.Optional.ofNullable(FIRST_VERTRAG_ID), java.util.Optional.ofNullable(editedFirstMessage.getId()));
		Assert.assertEquals(1,editedFirstMessage.getProjekt());
	}

	@Test
	@WithMockUser(roles = {"EDITOR"})
	public void
	givenRoleEditor_whenFind1stMessageByIdAndUpdateContent_thenFail(){
		AccessDeniedException accessDeniedException = Assertions.assertThrows(AccessDeniedException.class, () -> {
			Vertrag vertrag = repo.findById(FIRST_VERTRAG_ID).orElseThrow();

			Assert.assertNotNull(vertrag);
			Assert.assertEquals(java.util.Optional.ofNullable(FIRST_VERTRAG_ID),java.util.Optional.ofNullable(vertrag.getId()));

			vertrag.setProjekt(1);
			vertraegeRestController.updateVertrag(vertrag);
		});
		Assert.assertEquals(AccessDeniedException.class, accessDeniedException.getClass());
	}

	@Test
	@WithMockUser(username = "hr")
	public void givenUsernameHr_whenFindMessageById2_thenOK(){
		Vertrag secondVertrag = repo.findById(SECOND_VERTRAG_ID).orElseThrow();
		Assert.assertNotNull(secondVertrag);
		Assert.assertEquals(java.util.Optional.ofNullable(SECOND_VERTRAG_ID),java.util.Optional.ofNullable(secondVertrag.getId()));
	}

	@Test // todo: schlägt fehl weil save noch kein hasPermission hat
	@WithMockUser(username = "hr")
	public void givenUsernameHr_whenUpdateMessageWithId2_thenFail(){
		AccessDeniedException accessDeniedException = Assertions.assertThrows(AccessDeniedException.class, () -> {
			Vertrag vertrag = new Vertrag();
			vertrag.setName("test");
			vertrag.setId(SECOND_VERTRAG_ID);
			vertrag.setProjekt(1);
			vertraegeRestController.saveVertrag(vertrag);
		});
		Assert.assertEquals(AccessDeniedException.class, accessDeniedException.getClass());
	}

//	@Test
//	@WithMockUser(username = "manager")
//	public void givenUsernameMangager_whenUpdateMessageWithId2(){
//			Vertrag secondMessage = new Vertrag();
//			secondMessage.setId(SECOND_VERTRAG_ID);
//			secondMessage.setProjektnummer(EDITTED_CONTENT);
//			repo.save(secondMessage);
//	}
}
